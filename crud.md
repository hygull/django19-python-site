CREATE - POST  => (Make new)
READ - GET => (Get details)
UPDATE - PUT/PATCH => (Edit details)
DELETE - Delete => (Destroy details)