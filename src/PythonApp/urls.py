from django.conf.urls import url


from .views import (
	post_list,
	post_create,
	post_update,
	post_detail,
	post_delete
)

urlpatterns = [
    url(r"^$",post_list,name="list"),
    url(r"^create$",post_create),
    url(r"^(?P<id>\d+)/edit/$",post_update),
    url(r"^(?P<id>\d+)/$",post_detail,name='detail'),# url(r"^detail$",post_detail),
    url(r"^(?P<id>\d+)/delete$",post_delete),
]

